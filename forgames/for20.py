from aiogram import Bot, types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.dispatcher import Dispatcher
from aiogram.utils import executor
from keyboards import cs_client, eu_client
from token_tg import TOKEN




bot = Bot(token=TOKEN)
dp = Dispatcher(bot)
IDS_CS = {"39*****81": "John", 
       "401*****9": "George"}

IDS_EU = {"6******752": "Oleg",
       "38******63": "Petro", }



cs_database = set()
eu_database = set()

@dp.message_handler(commands=['go_cs'])
async def cs_message(message: types.Message):
    # await message.delete()
    cs_database.clear()
    global CS_PLAYERS
    CS_PLAYERS = "21:30 CS team: "
    for i in IDS_CS:
        await bot.send_sticker(f"{i}", "CAACAgIAAxkBAAEEGeFiKP_iFtb5e94CvifFhZbgDBuZuQACKwAD3yU9GeGJAWrpEwl-IwQ")
        await bot.send_message(f"{i}", f"{IDS_CS[i]}, go CS_GO!", reply_markup=cs_client)

@dp.message_handler(commands=['Yes_CS'])
async def play_cs_yes(message: types.Message):
        global CS_PLAYERS
        global IDS_CS 
        if len(cs_database) == 5:
            await bot.send_message(message.from_user.id, "Sorry, full team")
            await bot.send_sticker(message.from_user.id, "CAACAgIAAxkBAAEEJkRiLiHkpIhMTODzOPDvPqZQvJ0BiAACcgMAAm2wQgN8hjxqMH187iME")
        else:
            if message.from_user.id in cs_database:
                await bot.send_message(message.from_user.id, "Stop touch YES") 
            else:
                cs_database.add(message.from_user.id)
                b = message.from_user.id
                CS_PLAYERS += f"\n{IDS_CS[str(b)]}"
                await bot.send_sticker(message.from_user.id, "CAACAgIAAxkBAAEEHkliK5DyUXP0oDlYoOwQgRmD3IhQYQACww8AApkVIUsFKVnmJxtr-SME")
                if len(cs_database) == 5:
                    await bot.send_message("-767970580", CS_PLAYERS)
            

@dp.message_handler(commands=['No_CS'])
async def play_cs_no(message: types.Message):
#        await message.delete()
        global IDS_CS 
        namename = IDS_CS[str(message.from_user.id)]
        await bot.send_sticker(message.from_user.id, "CAACAgIAAxkBAAEEHktiK5D87jWw-ZkSJTK0DGOhYkTu9QACLgwAAuobKUvxqtW-ITJskyME")
        await bot.send_message("-767970580", f"{namename}, will skip CS")

@dp.message_handler(commands=['show_cs'])
async def show_cs(message: types.Message):
    global CS_PLAYERS
    await bot.send_message("-767970580", CS_PLAYERS)

@dp.message_handler(commands=['go_eu'])
async def eu_message(message: types.Message):
#    await message.delete()
    eu_database.clear()
    global EU_PLAYERS
    EU_PLAYERS = "Europe team: " 
    for i in IDS_EU:
        await bot.send_sticker(f"{i}", "CAACAgIAAxkBAAEEGeFiKP_iFtb5e94CvifFhZbgDBuZuQACKwAD3yU9GeGJAWrpEwl-IwQ")
        await bot.send_message(f"{i}", f"{IDS_EU[i]}, go Europe!", reply_markup=eu_client)



@dp.message_handler(commands=['Yes_EU'])
async def play_eu_yes(message: types.Message):
#        await message.delete()
        global IDS_EU
        global EU_PLAYERS
        if message.from_user.id in eu_database:
            await bot.send_message(message.from_user.id, "Stop touch YES") 
        else:
            eu_database.add(message.from_user.id)
            b = message.from_user.id
            EU_PLAYERS += f"\n{IDS_EU[str(b)]}"
            await bot.send_sticker(message.from_user.id, "CAACAgIAAxkBAAEEHkliK5DyUXP0oDlYoOwQgRmD3IhQYQACww8AApkVIUsFKVnmJxtr-SME")
            await bot.send_sticker(message.from_user.id, "CAACAgIAAxkBAAEEHkliK5DyUXP0oDlYoOwQgRmD3IhQYQACww8AApkVIUsFKVnmJxtr-SME")

@dp.message_handler(commands=['show_eu'])
async def show_eu(message: types.Message):
    global EU_PLAYERS
    await bot.send_message("-767970580", EU_PLAYERS)

@dp.message_handler(commands=['No_EU'])
async def play_eu_no(message: types.Message):
        global IDS_EU
        namename = IDS_EU[str(message.from_user.id)]
        await bot.send_sticker(message.from_user.id, "CAACAgIAAxkBAAEEHktiK5D87jWw-ZkSJTK0DGOhYkTu9QACLgwAAuobKUvxqtW-ITJskyME")
        await bot.send_message("-767970580", f"{namename}, will skip Europe")
if __name__ == '__main__':
    executor.start_polling(dp)
