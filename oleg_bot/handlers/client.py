from aiogram import types, Dispatcher
from create_bot import dp, bot
from keyboards import kb_client
from data_base import sqlite_db


# @dp.message_handler(commands=['start'])
async def send_welcome(message: types.Message):
    try:
        await bot.send_message(message.from_user.id, "Hi!\nI'm HookaBot!\nPowered by olegyura.\nYou can use /help to additional information", reply_markup=kb_client)
        await message.delete()
    except:
        await message.reply('At first you should start conversation with bot:\n@Shisha_spawnBot')

# @dp.message_handler(commands=['help'])
async def send_help(message: types.Message):
    await message.answer("Help command!\nfor you")

# @dp.message_handler(commands=['/Menu'])
async def send_menu(message: types.Message):
    await sqlite_db.sql_read(message)
# @dp.message_handler()
async def echo(message: types.Message):
    await message.answer(message.text)
# @dp.message_handler()
# async def echo_message(msg: types.Message):
#     await bot.send_message(msg.from_user.id, msg.text)


def register_handlers_client(dp : Dispatcher):
    dp.register_message_handler(send_welcome, commands=['start'])
    dp.register_message_handler(send_help, commands=['help'])
    dp.register_message_handler(send_menu, commands=['Menu'])
    dp.register_message_handler(echo)