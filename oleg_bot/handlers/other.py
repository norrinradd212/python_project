from aiogram import types, Dispatcher
from create_bot import dp
import json, string

async def echo_send(message : types.Message):
    mat = ["мат", "срака"]
    if {i.lower().translate(str.maketrans('', '', string.punctuation)) for i in message.text.split(' ')}\
        .intersection(set(json(["мат", "срака"]))) != set():
        await message.reply("Матюкатись не можна")

def register_henadlers_other(dp : Dispatcher):
    dp.register_message_handler(echo_send)